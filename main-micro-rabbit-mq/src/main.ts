import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');
  let port = 8081;
  app.enableCors();
  await app.listen(port, () => {
    console.log('App Listening At Port ' + port);
  });
}
bootstrap();
