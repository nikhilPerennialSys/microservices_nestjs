import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/microservices';

async function bootstrap() {
  // const app = await NestFactory.create(AppModule);
  // app.setGlobalPrefix('api');
  // app.enableCors();
  // await app.listen(8081);
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: [
        'amqps://lbpzeyzl:J5GWsG_Zd3qMN2QMSWbwvFbsLKvz_E2a@puffin.rmq2.cloudamqp.com/lbpzeyzl',
      ],
      queue: 'main_queue',
      queueOptions: {
        durable: false,
      },
    },
  });

  app.listen().then(() => {
    console.log('Listening To Microservices');
  });
  // amqps://lbpzeyzl:J5GWsG_Zd3qMN2QMSWbwvFbsLKvz_E2a@puffin.rmq2.cloudamqp.com/lbpzeyzl
}
bootstrap();
