import { Controller, Get, NotFoundException } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { Product } from './product.model';
import { ProductService } from './product.service';

@Controller('products')
export class ProductController {
  constructor(private readonly prodServices: ProductService) {}
  @Get('getAll')
  async getAll() {
    try {
      const products = await this.prodServices.all();
      if (products.length < 1) {
        throw new NotFoundException('No Products Found');
      } else {
        return { success: true, message: 'All Products', products };
      }
    } catch (error) {
      throw error;
    }
  }

  @EventPattern('product_created')
  async createProduct(product: any) {
    try {
      const savedProd = await this.prodServices.createProd(product);
      console.log(savedProd);
    } catch (error) {
      throw error;
    }
  }

  @EventPattern('product_updated')
  async updateProduct(data: { prodID: number; updatedProduct: any }) {
    try {
      const updatedProd = await this.prodServices.updateProd(
        data.prodID,
        data.updatedProduct,
      );
      console.log(updatedProd);
    } catch (error) {
      throw error;
    }
  }

  @EventPattern('product_deleted')
  async deleteProduct(prodID: number) {
    try {
      const deletedProd = await this.prodServices.deleteProd(prodID);
      console.log(deletedProd);
    } catch (error) {
      throw error;
    }
  }
}
