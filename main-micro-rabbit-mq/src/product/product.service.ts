import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Product, ProductDocument, productSchema } from './product.model';

@Injectable()
export class ProductService {
  constructor(
    @InjectModel(Product.name)
    private readonly productModel: Model<ProductDocument>,
  ) {}

  async all() {
    try {
      const allProds = await this.productModel.find({});
      console.log(allProds);
      return allProds;
    } catch (error) {
      throw error;
    }
  }

  async createProd(prod: any) {
    try {
      const product = await new this.productModel(prod);
      await product.save();
      return product;
    } catch (error) {
      throw error;
    }
  }

  async updateProd(prodID: number, prod: any) {
    try {
      const product = await this.productModel.findOneAndUpdate(
        { id: prodID },
        prod,
      );
      await product.save();
      const updated = await this.productModel.findOne({ id: prodID });
      return updated;
    } catch (error) {
      throw error;
    }
  }

  async deleteProd(prodID: number) {
    try {
      const product = await this.productModel.findOneAndDelete({ id: prodID });
      return product;
    } catch (error) {
      throw error;
    }
  }
}
