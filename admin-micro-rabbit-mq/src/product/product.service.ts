import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { allowedNodeEnvironmentFlags } from 'process';
import { Repository } from 'typeorm';
import { Product } from './product.entity';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
  ) {}

  async all() {
    try {
      const products = await this.productRepository.find();
      return products;
    } catch (error) {
      throw error;
    }
  }

  async create(data: { title: string; image: string }) {
    try {
      const productToSave = await this.productRepository.save(data);
      return productToSave;
    } catch (error) {
      throw new BadRequestException(error.driverError.sqlMessage);
    }
  }

  async deleteProd(prodID: number) {
    try {
      const deletedProd = await this.productRepository.delete({ id: prodID });
      return deletedProd;
    } catch (error) {
      throw error;
    }
  }

  async updateProduct(data: { title: string; image: string }, id) {
    try {
      const updated = await this.productRepository.update(id, data);
      return updated;
    } catch (error) {
      throw error;
    }
  }

  async getSingleProd(prodID: number) {
    try {
      const prod = await this.productRepository.findOne(prodID);
      return prod;
    } catch (error) {
      throw error;
    }
  }
}
