import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpService,
  Inject,
  NotFoundException,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { ProductService } from './product.service';

@Controller('products')
export class ProductController {
  constructor(
    private readonly productService: ProductService,
    @Inject('PRODUCT_SERVICE') private readonly client: ClientProxy,
  ) {}
  @Get('getAll')
  async getAll() {
    try {
      this.client.emit('hello', 'Hello From RabbitMQ');
      const products = await this.productService.all();
      if (products.length > 0) {
        return { success: true, products };
      } else {
        throw new NotFoundException();
      }
    } catch (error) {
      throw new NotFoundException('No Products Found !');
    }
  }

  @Post('createProduct')
  async create(@Body() data: { title: string; image: string }) {
    try {
      if (!data.title) {
        throw new BadRequestException('Title Is Required !');
      }
      if (!data.image) {
        throw new BadRequestException('Image Is Required !');
      }
      const created = await this.productService.create(data);
      if (created.id) {
        this.client.emit('product_created', created);
        return { success: true, message: `Product Created`, product: created };
      } else {
        throw new BadRequestException('Could Not Create Product !');
      }
    } catch (error) {
      throw error;
    }
  }

  @Delete('deleteProd/:id')
  async deleteProduct(@Param('id') prodID: number) {
    try {
      if (!prodID) {
        throw new Error('Please Provide Prouduct ID');
      }
      const deleted = await this.productService.deleteProd(prodID);
      if (deleted.affected === 1) {
        this.client.emit('product_deleted', prodID);
        return {
          success: true,
          message: `Product Deleted With Product ID : ${prodID}`,
        };
      } else {
        throw new NotFoundException('Product Not Found !');
      }
    } catch (error) {
      throw error;
    }
  }

  @Patch('update/:id')
  async updateProd(
    @Param('id') id: number,
    @Body() data: { title: string; image: string },
  ) {
    try {
      if (!data.title || !data.image) {
        throw new BadRequestException('Title Or Image Is Required !');
      }
      const updated = await this.productService.updateProduct(data, id);
      if (updated.affected === 1) {
        const singleProd = await this.productService.getSingleProd(id);
        const data = {
          prodID: id,
          updatedProduct: singleProd,
        };
        this.client.emit('product_updated', data);
        return {
          success: true,
          message: `Product With ID ${id} updated`,
          updatedData: singleProd,
        };
      } else {
        throw new BadRequestException('Could Not Found Product To Update !');
      }
    } catch (error) {
      throw error;
    }
  }

  @Get('getOne/:id')
  async getByID(@Param('id') id: number) {
    try {
      const singleProd = await this.productService.getSingleProd(id);
      if (!singleProd) {
        throw new NotFoundException(`Product With ID ${id} Not Found !`);
      } else {
        return {
          success: true,
          message: `Product Found With ID ${id}`,
          product: singleProd,
        };
      }
    } catch (error) {
      throw error;
    }
  }
}
